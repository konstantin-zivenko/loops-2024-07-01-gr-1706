# Вивести у консоль перші 100 чисел Фібоначчі (див. у рекомендованих ресурсах)
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

# 0 - current = 0, next
# 1 - next_num = 1,
# 2 - current + next_num

print(0)
print(1)
current = 0
next_num = 1
for i in range(2, 100):
    current, next_num = next_num, current + next_num
    print(next_num)
